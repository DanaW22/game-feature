using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace GCTHREEPROne
{
	// This Script is used for the Start Menu of the feature
	public class Menu : MonoBehaviour
	{
		[SerializeField]private GameObject team;
		[SerializeField]private GameObject packages;
		
		
		
		void Start()
		{ 
			// At the start, different Game Objects get deactived and the Player
		 // uses the UI input of the menu to activate different objects to continue
		 
			team.SetActive(false);
			packages.SetActive(false);
		}


		public void StartFeature()
		{
			SceneManager.LoadSceneAsync("Scene 1");
		
		}
		
		
		public void OpenTeamText()
		{
			team.SetActive(true);
		}

		public void CloseTeamText()
		{
			team.SetActive(false);
		}

		public void OpenPackageText()
		{
			packages.SetActive(true);
		}

		public void ClosePackageText()
		{
			packages.SetActive(false);
		}

	}
}

