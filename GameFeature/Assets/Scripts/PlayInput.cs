using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace GCTHREEPROne
{ 
    //This Script is used for the player input in the scenes
    public class PlayInput : MonoBehaviour
    {
        //This describes the name of the next scene, that will be activated
        [SerializeField] private string nextScene;
       
        
        void Update()  
        {  
            ChangeScene();  
        }  
  
        private void ChangeScene()  
        {  
            // With this the input is set to the currently used keyboard on the played devices
            // and if the Enter Key is pressed, the new scene will be loaded
            Keyboard myKeyboard = Keyboard.current;  
  
            if (myKeyboard.enterKey.isPressed)  
            {  
                SceneManager.LoadScene(nextScene);
                
            }

        }  
    }

}

